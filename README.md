# Projeto Heeler

<div align="center">   
   

   <h1>
      ![alt text](heeler_logo1.png "Heeler Logo")
      
   </h1>

</div>


# Monitoramento de nível de água em bebedouros australianos para criações


Monitorar nível de água de bebedouros para criação de gado. Mostrar nível do bebedouro e emitir alertas caso o nível alcance o mínimo e máximo de água no tanque.


- Nível : Apresentará em dashboard os níveis de 50% e 100%.

- Nível mínimo: Apresentará em dashboard, alerta por falta de água no tanque.
- Nível máximo: Apresentará em dashboard, alerta para transbordamento de água no tanque.

O projeto visa agilizar o tempo de diagnóstico dos tanques dentro de uma propriedade de médio e grande porte.


# Motivos:

- Obstrução do encanamento condutor de água.

- Quebra da boia.
- Quebra da bomba.

# Cronograma:

<div align="left">   
   

   <h1>
      ![alt text](cronograma1.png "Cronograma")
      
   </h1>

</div>
