import React from 'react';
import { useAuth0 } from '@auth0/auth0-react';
import logo from './logo_heeler_login.png';

const Dashboard = () => {
    const{isAuthenticated} = useAuth0();

    return(
        isAuthenticated &&(
        <div>        
        <img src = { logo } className = "App-Logo-Dash" alt = "logo" /> 
        <h2 className = "App">Bebedouro</h2>
        </div>
        )
    )
}
export default Dashboard