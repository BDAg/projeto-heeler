import React, {useState} from 'react';
import {
ProSidebar,
Menu,
MenuItem,
SidebarHeader,
SidebarFooter,
SidebarContent,
} from "react-pro-sidebar";
import{FaList, FaChartBar} from "react-icons/fa";
import { FiHome, FiLogOut, FiArrowLeftCircle, FiArrowRightCircle } from "react-icons/fi";
import { RiMapPinLine } from "react-icons/ri";
import {MdLocalDrink} from "react-icons/md";
import "react-pro-sidebar/dist/css/styles.css";
import "./sidebar.css";
import { useAuth0 } from '@auth0/auth0-react';
//import logo from "./components/logo192.png";
//import LogoutButton from './components/LogoutButton';

const Header = () => {
    const{user, logout, isAuthenticated} = useAuth0();
    
  
    //create initial menuCollapse state using useState hook
    const [menuCollapse, setMenuCollapse] = useState(false)

    //create a custom function that will change menucollapse state from false to true and true to false
  const menuIconClick = () => {
    //condition checking to change state from true to false and vice versa
    menuCollapse ? setMenuCollapse(false) : setMenuCollapse(true);
  };

  return (
    isAuthenticated && (    
    
    <>
      <div id="header">
          {/* collapsed props to change menu size using menucollapse state */}
        <ProSidebar collapsed={menuCollapse}>
          <SidebarHeader>
        <div className = "image">
        <img src={user.picture} className = "image" alt= {user.name} />
        </div>
        <div className="logotext">
              {/* small and big change using menucollapse state */}            
            <p>{menuCollapse ? user.name.toLocaleLowerCase: user.name}</p>                
            </div>
            <div className="closemenu" onClick={menuIconClick}>
                {/* changing menu collapse icon on click */}
              {menuCollapse ? (
                <FiArrowRightCircle/>
              ) : (
                <FiArrowLeftCircle/>
              )}
            </div>
          </SidebarHeader>
          <SidebarContent>
            <Menu iconShape="square">
              <MenuItem active={true} icon={<FiHome />}>
                Home
              </MenuItem>
              <MenuItem icon={<FaList />}>Relatório</MenuItem>
              <MenuItem icon={<MdLocalDrink />}>Bebedouro</MenuItem>
              <MenuItem icon={<RiMapPinLine />}>Map</MenuItem>
              <MenuItem icon={<FaChartBar />}>Histórico</MenuItem>
            </Menu>
          </SidebarContent>
          <SidebarFooter>
            <Menu iconShape="square" onClick={() => logout()}>
              <MenuItem icon={<FiLogOut />}>Logout</MenuItem>
            </Menu>
          </SidebarFooter>
        </ProSidebar>
      </div>
        
    </>
    
    )
  );
};

export default Header;