import React from 'react';
import { useAuth0 } from '@auth0/auth0-react';
import Header from './sidebar';

const LogoutButton = () => {
    const{logout, isAuthenticated} = useAuth0();

    return(
        isAuthenticated &&(
        <div>        
        <button className = "logout_button" onClick={() => logout()}>
        Log Out        
        </button>
        </div>
        )
    )
}
export default LogoutButton