import React from 'react';
import { useAuth0 } from '@auth0/auth0-react';
import logo from './logo_heeler_login.png';
import { Button } from 'react-responsive-button';
import 'react-responsive-button/dist/index.css';

const LoginButton = () => {
    const{loginWithRedirect, isAuthenticated} = useAuth0();

    return(
        !isAuthenticated &&(
        <div>        
            <body className = "App-body">
                <h2> Welcome </h2>    
                <img src = { logo } className = "App-logo" alt = "logo"/>                               
            </body>
                <Button text className= "button"  onClick={() => loginWithRedirect()}>
                    Log In
                </Button>
        </div>
            
        )
    )
}
export default LoginButton